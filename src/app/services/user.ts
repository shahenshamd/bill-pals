export interface User {
    uid: string;
    email: string;
   
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    
}
export interface UserProfile {
    email: string;
    fullName: string;
    isAdmin:boolean;
    acnikeName:string;
  
  }