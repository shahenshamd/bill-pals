import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RepaybillPage } from './repaybill.page';

const routes: Routes = [
  {
    path: '',
    component: RepaybillPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RepaybillPageRoutingModule {}
