import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RepaybillPageRoutingModule } from './repaybill-routing.module';

import { RepaybillPage } from './repaybill.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RepaybillPageRoutingModule
  ],
  declarations: [RepaybillPage]
})
export class RepaybillPageModule {}
