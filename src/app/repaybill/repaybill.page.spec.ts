import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RepaybillPage } from './repaybill.page';

describe('RepaybillPage', () => {
  let component: RepaybillPage;
  let fixture: ComponentFixture<RepaybillPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepaybillPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RepaybillPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
