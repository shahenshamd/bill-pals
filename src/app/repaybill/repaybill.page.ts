import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular'; 


@Component({
  selector: 'app-repaybill',
  templateUrl: './repaybill.page.html',
  styleUrls: ['./repaybill.page.scss'],
})
export class RepaybillPage implements OnInit {
  constructor(
    public alertCtrl: AlertController
  ) {
   
  }
  async showAlert() {  
    const alert = await this.alertCtrl.create({  
       
      message: 
      '<p>'+'Congratulations! You qualify for the following options'+'</p>'+'<br>'+
      '<ion-grid>'+
 ' <ion-row>'+
    '<ion-col>'+
      '<div>'+
        'Loan Duration'+
      '</div>'+
    '</ion-col>'+
    '<ion-col>'+
      '<div>'+
        'Fee'+
      '</div>'+
    '</ion-col>'+
    '<ion-col>'+
      '<div>'+
        'Due date'+
     ' </div>'+
   ' </ion-col>'+
   '<ion-col>'+
      '<div>'+
        'Total payback amount  on due date'+
     ' </div>'+
   ' </ion-col>'+
   '<ion-col>'+
      '<div>'+
        'Choose'+
     ' </div>'+
   ' </ion-col>'+
  '</ion-row>'+
  ' <ion-row>'+
  '<ion-col>'+
    '<div>'+
      '15 days'+
    '</div>'+
  '</ion-col>'+
  '<ion-col>'+
    '<div>'+
      '$10'+
    '</div>'+
  '</ion-col>'+
  '<ion-col>'+
    '<div>'+
      'Calculated  date'+
   ' </div>'+
 ' </ion-col>'+
 '<ion-col>'+
    '<div>'+
      'Calculated Loan '+'$10'
   +' </div>'+
 ' </ion-col>'+
 '<ion-col>'+
    '<div>'+
      '<a>Select>></a>'+
   ' </div>'+
 ' </ion-col>'+
'</ion-row>'+
'</ion-grid>'
      

    });  
    await alert.present();  
    const result = await alert.onDidDismiss();  
    console.log(result);  
  }    
   

  ngOnInit() {
  }

}
