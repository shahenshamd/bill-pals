import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {
  currentPageTitle = 'Dashboard';

  appPages = [
    {
      title: 'Home',
      url: '/dashboard',
      icon: 'home'
    },
    {
      title: 'Pay a Bill ',
      url: '/paybill',
      icon: 'card'
    },
    {
      title: 'Request a Loan',
      url: '/requestloan',
      icon: 'create'
    },
    {
      title: 'My Loan(s)',
      url: '/myloan',
      icon: 'copy'
    },
    {
      title: 'View History',
      url: '/history',
      icon: 'document'
    },
    {
      title: 'Add Bank Account',
      url: '/bank',
      icon: 'add'
    },
    {
      title: 'Repay Bill Pals',
      url: '/repaybill',
      icon: 'repeat'
    },
    {
      title: 'Logout',
      url: '/',
      icon: 'log-out'
    }


  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

    
}
