import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {  MenuController } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  validations_form: FormGroup;
  errorMessage: string = '';

  validation_messages = {
   'email': [
     { type: 'required', message: 'Email is required.' },
     { type: 'pattern', message: 'Please enter a valid email.' }
   ],
   'password': [
     { type: 'required', message: 'Password is required.' },
     { type: 'minlength', message: 'Password must be at least 5 characters long.' }
   ]
 };


  ionViewWillEnter() {
    this.menuCtrl.enable(false);
   }
  constructor(private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    public menuCtrl: MenuController) { }
    ngOnInit() {
      this.validations_form = this.formBuilder.group({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
        password: new FormControl('', Validators.compose([
          Validators.minLength(5),
          Validators.required
        ])),
      });
    }

  goRegisterPage(){
    this.router.navigate(["/signup"]);
  }
  goLoginPage(){
    this.router.navigate(["/login"]);
  }
  login(formData: FormData){
    this.authService.login(formData["email"], formData["password"]);
  }
 

}
