import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {  MenuController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {


  goPaybillPage(){
    this.router.navigate(["/paybill"]);
  }
  goRequestloanPage(){
    this.router.navigate(["/requestloan"]);
  }
  goHistoryPage(){
    this.router.navigate(["/history"]);
  }
  goRepaybillPage(){
    this.router.navigate(["/repaybill"]);
  }
  goMyloanPage(){
    this.router.navigate(["/myloan"]);
  }
  gobankPage(){
    this.router.navigate(["/bank"]);
  }


  ionViewWillEnter() {
    this.menuCtrl.enable(true);
   }
  constructor( 
    private router: Router,
    private authservice: AuthService,
    public menuCtrl: MenuController) { }


    logout(){
      this.authservice.logOut();
      
    }
    
  ngOnInit() {
  }

}
