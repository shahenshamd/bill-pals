import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,

    
    DashboardPageRoutingModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardPage
      }
    ])

  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
