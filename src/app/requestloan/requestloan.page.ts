import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { AlertController } from '@ionic/angular';  
@Component({
  selector: 'app-requestloan',
  templateUrl: './requestloan.page.html',
  styleUrls: ['./requestloan.page.scss'],
})
export class RequestloanPage implements OnInit {
  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
   };

  constructor(
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController
  ) { }
  async showAlert() {  
    const alert = await this.alertCtrl.create({  
       
      message: 
      '<p>'+'Congratulations! You qualify for the following options'+'</p>'+'<br>'+
      '<ion-grid>'+
 ' <ion-row>'+
    '<ion-col>'+
      '<div>'+
        'Loan Duration'+
      '</div>'+
    '</ion-col>'+
    '<ion-col>'+
      '<div>'+
        'Fee'+
      '</div>'+
    '</ion-col>'+
    '<ion-col>'+
      '<div>'+
        'Due date'+
     ' </div>'+
   ' </ion-col>'+
   '<ion-col>'+
      '<div>'+
        'Total payback amount  on due date'+
     ' </div>'+
   ' </ion-col>'+
   '</ion-row>'+
'</ion-grid>'
      

    });  
    await alert.present();  
    const result = await alert.onDidDismiss();  
    console.log(result);  
  }
  ngOnInit() {

    

  }

}
