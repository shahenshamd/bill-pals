import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RequestloanPage } from './requestloan.page';

describe('RequestloanPage', () => {
  let component: RequestloanPage;
  let fixture: ComponentFixture<RequestloanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestloanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RequestloanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
