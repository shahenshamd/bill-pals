import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestloanPage } from './requestloan.page';

const routes: Routes = [
  {
    path: '',
    component: RequestloanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestloanPageRoutingModule {}
