import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';  
import { AuthService } from '../services/auth.service';

import {  ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-paybill',
  templateUrl: './paybill.page.html',
  styleUrls: ['./paybill.page.scss'],
})
export class PaybillPage implements OnInit {
  items: Array<any>;
  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';

  validation_messages = {
   'email': [
     { type: 'required', message: 'Email is required.' },
     { type: 'pattern', message: 'Enter a valid email.' }
   ],
   'password': [
     { type: 'required', message: 'Password is required.' },
     { type: 'minlength', message: 'Password must be at least 5 characters long.' }
   ]
  };



  constructor(
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    private toastController: ToastController, 
    public LoadingController: LoadingController, 
    private fireauth: AngularFireAuth,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute

  ) { 

    
  }
 


  async showAlert() {  
    const alert = await this.alertCtrl.create({  
       
      message: 
      '<p>'+'Congratulations! You qualify for the following options'+'</p>'+'<br>'+
      '<ion-grid>'+
 ' <ion-row>'+
    '<ion-col>'+
      '<div>'+
        'Loan Duration'+
      '</div>'+
    '</ion-col>'+
    '<ion-col>'+
      '<div>'+
        'Fee'+
      '</div>'+
    '</ion-col>'+
    '<ion-col>'+
      '<div>'+
        'Due date'+
     ' </div>'+
   ' </ion-col>'+
   '<ion-col>'+
      '<div>'+
        'Total payback amount  on due date'+
     ' </div>'+
   ' </ion-col>'+
   '</ion-row>'+
'</ion-grid>'
      

    });  
    await alert.present();  
    const result = await alert.onDidDismiss();  
    console.log(result);  
  }  
  ngOnInit() {
    if (this.route && this.route.data) {
      this.getData();
    }
  }

  
  async getData(){
    const loading = await this.LoadingController.create({
      message: 'Please wait...'
    });
    this.presentLoading(loading);

    this.route.data.subscribe(routeData => {
      routeData['data'].subscribe(data => {
        loading.dismiss();
        this.items = data;
      })
    })
  }

  async presentLoading(loading) {
    return await loading.present();
  }




}
