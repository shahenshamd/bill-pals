import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';
import { LoadingController, ToastController } from '@ionic/angular';
@Component({
  selector: 'app-forget',
  templateUrl: './forget.page.html',
  styleUrls: ['./forget.page.scss'],
})
export class ForgetPage implements OnInit {
  email: string = '';
  error: string = '';
  constructor(private router: Router,
    private authService: AuthService,
    private fireauth: AngularFireAuth,
    private toastController: ToastController, 
    public alertController: AlertController
   ) { }

    validations_form: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';
  
    validation_messages = {
     'email': [
       { type: 'required', message: 'Email is required.' },
       { type: 'pattern', message: 'Enter a valid email.' }
     ],
    
    };  

    recover() {
      this.fireauth.auth.sendPasswordResetEmail(this.email)
        .then(data => {
          console.log(data);
          this.presentToast('Password reset email sent', false, 'bottom', 1000);
          this.router.navigateByUrl('/login');
        })
        .catch(err => {
          console.log(` failed ${err}`);
          this.error = err.message;
        });
    }
  
    ngOnInit() {
   

  }
  async presentToast(message, show_button, position, duration) {
    const toast = await this.toastController.create({
      message: message,
      
      position: position,
      duration: duration
    });
    toast.present();
  }
  
}
