import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyloanPage } from './myloan.page';

describe('MyloanPage', () => {
  let component: MyloanPage;
  let fixture: ComponentFixture<MyloanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyloanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyloanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
