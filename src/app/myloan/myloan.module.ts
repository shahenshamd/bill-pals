import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyloanPageRoutingModule } from './myloan-routing.module';

import { MyloanPage } from './myloan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyloanPageRoutingModule
  ],
  declarations: [MyloanPage]
})
export class MyloanPageModule {}
