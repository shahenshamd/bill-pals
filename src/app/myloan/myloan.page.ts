import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';  

@Component({
  selector: 'app-myloan',
  templateUrl: './myloan.page.html',
  styleUrls: ['./myloan.page.scss'],
})
export class MyloanPage implements OnInit {

  constructor( public alertCtrl: AlertController,
    private toastController: ToastController, 
    public loadingController: LoadingController,) {
    
   }
  async showAlert() {  
    const alert = await this.alertCtrl.create({  
       
      message: 
      '<p>'+'Congratulations! You qualify for the following options'+'</p>'+'<br>'+
      '<ion-grid>'+
 
'</ion-grid>'
      

    });  
    await alert.present();  
    const result = await alert.onDidDismiss();  
    console.log(result);  
  }
  ngOnInit() {
  }

}
