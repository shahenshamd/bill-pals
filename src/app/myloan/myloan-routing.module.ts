import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyloanPage } from './myloan.page';

const routes: Routes = [
  {
    path: '',
    component: MyloanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyloanPageRoutingModule {}
