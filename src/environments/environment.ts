// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCd2akgncJPkB8FgAmDSIKIh4DJnKyqnyg",
    authDomain: "edyoukation-3f5b9.firebaseapp.com",
    databaseURL: "https://edyoukation-3f5b9.firebaseio.com",
    projectId: "edyoukation-3f5b9",
    storageBucket: "edyoukation-3f5b9.appspot.com",
    messagingSenderId: "739207551828",
    appId: "1:739207551828:web:7f3f707111ae9a1d2606c1",
    measurementId: "G-W0YHR1YCSF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
